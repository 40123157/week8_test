#coding: utf8
# 注意: 新增資料時, 至少要有學號資料
# 後續修改將針對各筆資料, 將增加一個唯一號碼的序號
# 後續將要增加資料刪除與查詢, 以及資料分頁
import cherrypy, sys, os
# os 用於 unlink 檔案 (即刪除檔案用)
class 個人資料處理(object):
    _cp_config = {
    # 配合 utf-8 格式之表單內容
    # 若沒有 utf-8 encoding 設定,則表單不可輸入中文
    'tools.encode.encoding': 'utf-8'
    }
    
    # 網際表單
    def index(self):
        return '''
        資料新增表單 <br /><br />
        <form method=\"post\" action=\"doAct\">
        
        學號:<input type=\"text\" name=\"stud_number\"><br />
        姓名:<input type=\"text\" name=\"stud_name\"><br />
        <input type=\"submit\" value=\"送出\">
        <input type=\"reset\" value=\"重填\">
        </form>
        '''+self.menuLink()
    index.exposed = True
    
    # updateForm 應該要透過資料表列中, 使用者選擇 index 序號, 然後程式由資料檔
    # 中調出對應的現有欄位資料, 然後以 value = ... 進行表單的內容後, 讓使用者進行修改
    def updateForm(self,stud_number=None):
        # 設法根據 stud_number 查出對應的欄位資料
        個人資料 = []
        資料計數 = 0
        # 因應檔案未建立時, 使用者按下讀取檔案的選項
        try:
            檔案 = open("c:/out.txt", 'r', encoding="utf-8")
        except IOError as e:
            outString = "沒有資料, 請先輸入資料!<br />"
            outString += self.menuLink()
            return outString
        # 利用上述的 try except 取代下列直接讀檔案, 因為若無檔案時會產生錯誤
        # 檔案 = open("c:\\temp\\c1w10out.txt", 'r', encoding="utf-8")
        for 行資料 in 檔案.read().split('\n'):
            if 行資料 != "":
                資料計數 += 1
                學號,姓名 = 行資料.split(",")
                個人資料.append((學號,姓名))
        檔案.close()
        資料序號 = 0
        for 個人子內容 in 個人資料:
            if(個人子內容[0] == stud_number):
                stud_name = 個人子內容[1]
        outString = '''
        資料更新表單 <br /><br />
        <form method=\"post\" action=\"doUpdate\">
        '''
        outString += "學號:<input type=\"text\" name=\"stud_number\" value=\""+str(stud_number)+"\"><br />"
        outString += "姓名:<input type=\"text\" name=\"stud_name\" value=\""+str(stud_name)+"\"><br />"
        outString += '''
        <input type=\"submit\" value=\"送出\">
        <input type=\"reset\" value=\"重填\">
        </form>
        '''+self.menuLink()
        return outString
        
    updateForm.exposed = True
    
    # 表單處理函式
    def doAct(self, stud_number=None, stud_name=None, \
                    school_dept=None, major=None, expt_salary=None):
        outString = ""
        outString +="學號:"+stud_number
        outString += "<br />"
        outString += "姓名:"+stud_name
        outString += "<br />"
        #
        # 最後列出連結功能表單
        outString += self.menuLink()
        # 將資料存入檔案
        self.saveData(stud_number, stud_name, \
                    school_dept, major, expt_salary)
        return outString
    doAct.exposed = True
    
    # 輸入資料存檔
    def saveData(self, stud_number=None, stud_name=None, \
                    school_dept=None, major=None, expt_salary=None):
        # 以 append 附加的方式將資料存入檔案, 以逗點隔開
        檔案 = open("c:/out.txt", 'a', encoding="utf-8")
        內容 = str(stud_number)+","+str(stud_name)+","\
        +str(school_dept)+","+str(major)+','+str(expt_salary)+"\n"
        檔案.write(內容)
        檔案.close()
        return str(stud_number)+","+str(stud_name)+","\
        +str(school_dept)+","+str(major)+','+str(expt_salary)+", 已經存檔"
    
    # 由資料檔案讀出, 並且進行排序或表格列印, 欄位資料加總等流程
    def readData(self, key_num=0, rev_order=False):
        個人資料 = []
        資料計數 = 0
        # 因應檔案未建立時, 使用者按下讀取檔案的選項
        try:
            檔案 = open("c:/out.txt", 'r', encoding="utf-8")
        except IOError as e:
            outString = "沒有資料, 請先輸入資料!<br />"
            outString += self.menuLink()
            return outString
        for 行資料 in 檔案.read().split('\n'):
            if 行資料 != "":
                資料計數 += 1
                學號,姓名 = 行資料.split(",")
                個人資料.append((學號,姓名))
        print(個人資料)
        #
        # 根據數列中各 tuple 中的學號(亦即 key data[0])進行排序
        # 個人資料排列後, 必須加以指定成變數, 否則個人資料的內容排序後並未改變
        #個人資料 = sorted(個人資料, key=lambda data: data[0], reverse=True)
        個人資料 = sorted(個人資料, key=lambda data: data[int(key_num)], reverse=int(rev_order))
        print(個人資料)
        outString = ""
        for 索引 in range(資料計數):
            for 內索引 in range(5):
                # 學號要加上更新連結
                if 內索引 == 0:
                    outString += "<a href=\"updateForm?stud_number="+str(個人資料[索引][內索引])+"\">"+str(個人資料[索引][內索引])+"</a>,"
                else:
                    outString += str(個人資料[索引][內索引])+","
            outString += "<br />"
        #
        # 最後列出連結功能表單
        outString += self.menuLink()
        return outString
        
    readData.exposed = True

    def doUpdate(self, stud_number=None, stud_name=None, \
                    school_dept=None, major=None, expt_salary=None):
        個人資料 = []
        資料計數 = 0
        檔案 = open("c:/out.txt", 'r', encoding="utf-8")
        for 行資料 in 檔案.read().split('\n'):
            if 行資料 != "":
                資料計數 += 1
                學號,姓名 = 行資料.split(",")
                個人資料.append([學號,姓名])
        檔案.close()
        # 先刪除舊檔案
        os.unlink("c:/out.txt")
        資料序號 = 0
        for 個人子內容 in 個人資料:
            if(個人子內容[0] == stud_number):
                # 設法更新對應此學號的資料
                個人子內容[1] = stud_name
                個人資料[資料序號] = 個人子內容
            資料序號 += 1
            # 將更新後的資料存入資料檔案
            # 以 append 的方式將資料存入檔案, 以逗點隔開
            檔案 = open("c:/out.txt", 'a', encoding="utf-8")
            內容 = str(個人子內容[0])+","+str(個人子內容[1])+","\
            +str(個人子內容[2])+","+str(個人子內容[3])+','+str(個人子內容[4])+"\n"
            檔案.write(內容)
            檔案.close()
        print(個人資料)
        #
        # 根據數列中各 tuple 中的學號(亦即 key data[0])進行排序
        # 個人資料排列後, 必須加以指定成變數, 否則個人資料的內容排序後並未改變
        個人資料 = sorted(個人資料, key=lambda data: data[0], reverse=True)
        print(個人資料)
        outString = ""
        for 索引 in range(資料計數):
            for 內索引 in range(5):
                outString += str(個人資料[索引][內索引])+","
            outString += "<br />"
        #
        # 最後列出連結功能表單
        outString += self.menuLink()
        return outString
        
    doUpdate.exposed = True
    
    # 退出函式
    def exiting(self):
        print("系統即將退出!")
        sys.exit()
    exiting.exposed = True
    
    # 連結功能表單
    def menuLink(self):
        return '''
        <br />
        <a href=\"index\">input</a>|
        <a href=\"readData\">正向排序</a>|
        <a href=\"readData?rev_order=1\">反向排序</a>|
        <a href=\"exiting\">exiting</a>
        <br />
        '''
 
cherrypy.server.socket_port = 8083
cherrypy.server.socket_host = '127.0.0.1'
cherrypy.quickstart(個人資料處理())