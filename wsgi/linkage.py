#coding: utf-8
import os
import sys
import cherrypy
import random
import math

    #@+others
    #@+node:ppython.20131221094631.1658: *3* index
    @cherrypy.expose
    def linkage(self, h=None):
        # 將標準答案存入 answer session 對應區
        超文件輸出 = "<form method=POST action=doCheck>"
        超文件輸出 += "有一根連桿, 兩端標示為 C 與 D, 且以為圓心旋轉, 假如 C 點座標為(100, 100), D 在 C 點的水平方向右方, 且 D 點座標為(150, 100)，<br />，此程式可算出旋轉後的D座標和每順時針旋轉0.5度的座標<br />請輸入逆時針旋轉角度<input type=text name=h><br \>"
        超文件輸出 += "<input type=submit value=計算>"
        超文件輸出 += "</form><br /><a href='/'>回首頁</a>"
        return 超文件輸出
    #@+node:ppython.20131221094631.1659: *3* default
    @cherrypy.expose
    def default(self, attr='default'):
        # 內建 default 方法, 找不到執行方法時, 會執行此方法
        return "Page not Found!"
    #@+node:ppython.20131221094631.1660: *3* doCheck
    @cherrypy.expose
    def doCheck(self, h=None):
        h = float(h)
        X = 150-(50-50*math.cos(math.radians(h)))
        Y = 100+50*math.sin(math.radians(h))
        for i in range(720):
            ra=math.radians(-0.5)
            x=150-(50-50*math.cos((i+1)*ra))
            y=100+50*math.sin((i+1)*ra)
            檔案 = open(data_dir+"dcoord.txt", "a", encoding="utf-8")
            檔案.write(str(x)+","+str(y)+","+"\n")
            檔案.close()

        超文件輸出 = "旋轉後的D點座標為("+str(X)+","+str(Y)+")"+"<br \>"
        超文件輸出 += "<form method=POST action=doCheck>"
        超文件輸出 += "再輸入一次旋轉角度<input type=text name=h><br \>"
        超文件輸出 += "<input type=submit value=計算>"
        超文件輸出 += "</form><br /><a href='/'>回首頁</a>"
        檔案 = open(data_dir+"dcoord.txt", "r", encoding="utf-8")
        數列 = []
        for 行資料 in 檔案:
            數列.append(行資料.strip())
        檔案.close()
        outstring = ""
        for 索引 in range(720):
            outstring += "順時針旋轉"+str((索引+1)*0.5)+"度後D的座標:("+數列[索引]+")<br />"
        return 超文件輸出+outstring+"<br />"




    #@-others
#@-others
# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"},
        '/downloads':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': data_dir+"/downloads"}
    }

if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(link(), config = application_conf)
    else:
        # 近端執行啟動
        '''
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        '''        
        cherrypy.quickstart(link(), config = application_conf)
#@-leo