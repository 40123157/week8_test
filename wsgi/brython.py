#@+leo-ver=5-thin
#@+node:ppython.20131221094631.1631: * @file brython.py
#coding: utf-8


#@@language python
#@@tabwidth -4

#@+<<declarations>>
#@+node:ppython.20131221094631.1632: ** <<declarations>> (brython)
import cherrypy
import os
import sys

# 確定程式檔案所在目錄, 在 Windows 有最後的反斜線
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))
if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
    sys.path.append(os.path.join(os.getenv("OPENSHIFT_REPO_DIR"), "wsgi"))
else:
    sys.path.append(_curdir)
#@-<<declarations>>
#@+others
#@+node:ppython.20131221094631.1633: ** class Brython
class Brython(object):
    '''
    用來導入 Brython 網際運算環境
    '''
    #@+others
    #@+node:ppython.20131221094631.1634: *3* index
    @cherrypy.expose
    def index(self):
        return '''
    <a href="/">回到首頁</a><br />
    <link rel="stylesheet" href="/static/console.css">
    <!-- 導入或不導入 ace.js 編輯器
    <script src="http://d1n0x3qji82z53.cloudfront.net/src-min-noconflict/ace.js" type="text/javascript">
    </script>
     -->
    <script type="text/javascript" src="/static/Brython1.4-20131221-111525/brython.js"></script>
    <script type="text/javascript">
    window.onload = function(){
        brython({debug:1, cache:'version'});
    }
    </script>
    <script type="text/python3" src="/static/editor.py"></script>

    <script type="text/python3">
    from browser import doc
    # 導入 sys 模組
    import sys
    # 將./static 目錄加入系統搜尋目錄
    sys.path.append("./static")
    # 導入位於 ./static 目錄下的 header.py
    #import header

    #qs_lang,language = header.show('../')

    # other translations

    trans = {
        'report_bugs':{'en':'Please report bugs in the '},
        'test_page':{'en':'Tests page'},
        'run':{'en':'run'},
        'clear':{'en':'clear'}
    }

    for key in trans:
        if key in doc:
            doc[key].html = trans[key].get(language,trans[key]['en'])

    # link to test page
    #tplink = doc['test_page']

    #if qs_lang:
        #tplink.href += '?lang=%s' %qs_lang

    def set_debug():
        v = doc['debug']
        if v.checked:
            __BRYTHON__.debug = 1
        else:
            __BRYTHON__.debug = 0
    </script>

    <script>
    function run_js(){
        var cons = document.getElementById("console")
        var jscode = cons.value
        var t0 = (new Date()).getTime()
        eval(jscode)
        var t1 = (new Date()).getTime()
        console.log("Javascript code run in "+(t1-t0)+" ms")
    }
    </script>
    <!-- 關掉 banner 按鈕
    <table id="banner" cellpadding=0 cellspacing=0>
    <tr id="banner_row">
    </tr>
    </table>
    -->

    <div style="text-align:center">
    <br>Brython version: <span id="version"></span>
    </div>
    </center>

    <div id="container">
    <div id="left-div">
    <div style="padding: 3px 3px 3px 3px;">
    Theme: <select id="ace_theme" onChange="change_theme(event)">
    <optgroup label="Bright">
    <option value="ace/theme/chrome">Chrome</option>
    <option value="ace/theme/clouds">Clouds</option>
    <option value="ace/theme/crimson_editor">Crimson Editor</option>
    <option value="ace/theme/dawn">Dawn</option>
    <option value="ace/theme/dreamweaver">Dreamweaver</option>
    <option value="ace/theme/eclipse">Eclipse</option>
    <option value="ace/theme/github">GitHub</option>
    <option value="ace/theme/solarized_light">Solarized Light</option>
    <option value="ace/theme/textmate">TextMate</option>
    <option value="ace/theme/tomorrow">Tomorrow</option>
    <option value="ace/theme/xcode">XCode</option>
    </optgroup>
    <optgroup label="Dark">
    <option value="ace/theme/ambiance">Ambiance</option>
    <option value="ace/theme/chaos">Chaos</option>
    <option value="ace/theme/clouds_midnight">Clouds Midnight</option>
    <option value="ace/theme/cobalt">Cobalt</option>
    <option value="ace/theme/idle_fingers">idleFingers</option>
    <option value="ace/theme/kr_theme">krTheme</option>
    <option value="ace/theme/merbivore">Merbivore</option>
    <option value="ace/theme/merbivore_soft">Merbivore Soft</option>
    <option value="ace/theme/mono_industrial">Mono Industrial</option>
    <option value="ace/theme/monokai">Monokai</option>
    <option value="ace/theme/pastel_on_dark">Pastel on dark</option>
    <option value="ace/theme/solarized_dark">Solarized Dark</option>
    <option value="ace/theme/twilight">Twilight</option>
    <option value="ace/theme/tomorrow_night">Tomorrow Night</option>
    <option value="ace/theme/tomorrow_night_blue">Tomorrow Night Blue</option>
    <option value="ace/theme/tomorrow_night_bright">Tomorrow Night Bright</option>
    <option value="ace/theme/tomorrow_night_eighties">Tomorrow Night 80s</option>
    <option value="ace/theme/vibrant_ink">Vibrant Ink</option>
    </optgroup>
    </select> 
    </div>
      <div id="editor"></div>
    </div>
    <div>
    <button onClick="reset_src()" id="reset src">reset src</button>
    <button onClick="clear_src()" id="clear src">clear src</button>
    <button onClick="clear_console()" id="clear console">clear console</button>
    </div>
    <div id="right-div">
    <div style="padding: 3px 3px 3px 3px;">
      <div style="float:left">
        <button onClick="run()" id="run Python">run</button>
        <button onClick="run_js()" id="clear">run Javascript</button>
        debug<input type="checkbox" id="debug" onchange="set_debug()" checked>
      </div>

      <div style="float:right">
        <button onClick="show_console()" id="show_console">Console</button>
        <button onClick="show_js()">Javascript</button>
      </div>
    </div>
    <div style="width:100%;height:100%;">
    <textarea id="console"></textarea>
    </div>
    </div>
    '''
    #@-others
#@-others
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"}
    }
    
if __name__ == '__main__':
    # 假如在 os 環境變數中存在 'OPENSHIFT_REPO_DIR', 表示程式在 OpenShift 環境中執行
    if 'OPENSHIFT_REPO_DIR' in os.environ.keys():
        # 雲端執行啟動
        application = cherrypy.Application(Brython(), config = application_conf)
    else:
        # 近端執行啟動
        '''
        cherrypy.server.socket_port = 8083
        cherrypy.server.socket_host = '127.0.0.1'
        '''
        cherrypy.quickstart(Brython(), config = application_conf)
#@-leo
